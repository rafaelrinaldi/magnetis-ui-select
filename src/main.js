/* jshint strict: false, jquery: true, newcap: false */
/* global Module */

/**
 * Magnetis » Dropdown menu
 *
 * TODO: Find a better way to grab the offset (aka don't use data attributes just for that).
 */
Module('DropdownMenu', function(DropdownMenu) {

  var instanceCount = 0;

  /**
   * Initialize the dropdown module.
   * @param {jQuery Object} target Dropdown container.
   */
  DropdownMenu.fn.initialize = function(target) {
    ++instanceCount;

    this.context = $(target);

    this.CLOSE_TIMEOUT = 175;

    this.menuToggle = this.context.find('.js-dropdown-menu-toggle');
    this.menuToggle.mouseover($.proxy(this._menuToggleMouseOverHandler, this));
    this.menuToggle.mouseout($.proxy(this._menuToggleMouseOutHandler, this));

    this.menuList = this.context.find('.js-dropdown-menu-list');
    this.menuList.mouseover($.proxy(this._menuListMouseOverHandler, this));
    this.menuList.mouseout($.proxy(this._menuListMouseOutHandler, this));
    this.menuList.css({zIndex: 9999 + instanceCount});

    this.offset = {
      top: this.context.data('offset-top') || 45
    };

    this.menuItems = this.context.find('.js-dropdown-menu-item');

    this._setupMenuItems();
  };

  DropdownMenu.fn._setupMenuItems = function() {
    var self = this;

    this.menuItems.each(function(index, element) {
      self._setupPosition(element);
    });
  };

  /**
   * Sadly CSS isn't smart enough so we need this to make sure
   * the menu list will always be displayed on the right place
   * regardless of toggle width or menu items width.
   * @param {jQuery object} target Current target.
   */
  DropdownMenu.fn._setupPosition = function(target) {
    // TODO: check if this condition is needed. Today is prevent an error to occur with IE9 on return report page
    if (!this.menuList.offset()) {
      return;
    }
    var target = $(target),
        position = target.data('position') || 'left',
        menu = target.find('.js-dropdown-menu-list'),
        toggle = target.find('.js-dropdown-menu-toggle'),
        toggleWidth = toggle.width(),
        menuWidth = menu.width(),
        targetLeft = this.menuList.position().left,
        offset = 0,
        toggleHalf = toggleWidth * .5;

    switch(position) {
      case 'left' :
        menu.offset({left: targetLeft + offset});
      break;

      case 'middle' :
        offset = -10;
        menu.offset({left: (targetLeft - (menuWidth * .5)) + toggleHalf + offset});
      break;

      case 'right' :
        offset = -8;
        menu.offset({left: targetLeft - menuWidth + toggleHalf + offset});
      break;
    }

    menu.css({top: this.offset.top});
  };

  /**
   * Open menu from a given context.
   * 1. Adjust menu position
   * 2. Add `is-active` class to the <li> container
   * @param {jQuery object} target Item container.
   */
  DropdownMenu.fn._openMenu = function() {
    var menu = this.activeItem.find('.js-dropdown-menu-list'),
        top = (this.offset.top - 5) + 'px';

    menu.fadeIn(100).animate({top: top}, {duration: 100, queue: false});
  };

  /**
   * Close menu from a given context.
   * 1. Remove `is-active` class from <li> container.
   * @param {jQuery object} target Item container.
   */
  DropdownMenu.fn._closeMenu = function() {
    var menu = this.activeItem.find('.js-dropdown-menu-list');

    menu
      .fadeOut(100)
      .animate({top: this.offset.top}, {duration: 100, queue: false});

    this.activeItem = null;
  };

  DropdownMenu.fn._menuToggleMouseOverHandler = function(event) {
    var target = $(event.currentTarget).parent();

    clearTimeout(this.closeMenuTimeout);

    if(this.activeItem && this.activeItem.index() !== target.index()) {
      this._closeMenu();
    }

    this.activeItem = target;
    this._openMenu();

    event.preventDefault();
  };

  DropdownMenu.fn._menuToggleMouseOutHandler = function() {
    var self = this;

    this.closeMenuTimeout = setTimeout(function() {
      self._closeMenu(this.activeItem);
    }, this.CLOSE_TIMEOUT);
  };

  DropdownMenu.fn._menuListMouseOverHandler = function(event) {
    clearTimeout(this.closeMenuTimeout);
    event.preventDefault();
  };

  DropdownMenu.fn._menuListMouseOutHandler = function(event) {
    var self = this;

    this.closeMenuTimeout = setTimeout(function() {
      self._closeMenu(this.activeItem);
    }, this.CLOSE_TIMEOUT);

    event.preventDefault();
  };
});
